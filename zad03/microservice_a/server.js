import axios from "axios";
import cors from "cors";
import express from "express";

const app = express();
const port = 3000;

app.use(cors());

app.get("/", async (_, res) => {
  const { data } = await axios.get("http://internal-api:3000/answear");
  res.json(data);
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}...`);
});
