import { MongoClient } from "mongodb";

const url = "mongodb://db-service:27017";
const client = new MongoClient(url);

const getDb = () => client.db("database");

export default getDb;
